/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void capFrameRate(long* then, float* remainder);

char* global_dir = "C:\\Users\\di918039\\source\\repos\\cs1pr-portfolio\\project-visual-studio\\SpringProject";

int main(){
	long then;
	float remainder;

	memset(&app, 0, sizeof(App));
	app.textureTail = &app.textureHead;

	initSDL();

	atexit(cleanup);

	initGame();

	initStage();

	then = SDL_GetTicks();

	remainder = 0;

	app.start = 0;

	app.level = 1;

	app.timer = 1;

	SDL_Texture* playButton;		//Initializing an SDL_Texture and naming it playButton
	playButton = IMG_LoadTexture(app.renderer, "gfx/PlayButton.png");		//Assign playbutton to the image of Play Button texture
	SDL_Texture* pizza;		//Initializing an SDL_Texture and naming it playButton
	pizza = IMG_LoadTexture(app.renderer, "gfx/pizza.png");			//Assign pizza to the image of pizza texture	
	SDL_Texture* mapButton;			//Initializing an SDL_Texture and naming it map Button
	mapButton = IMG_LoadTexture(app.renderer, "gfx/MapButton.png");		//Assign map button to the image of map button texture		
	SDL_Texture* exitButton;		//Initializing an SDL_Texture and naming it Exit button
	exitButton = IMG_LoadTexture(app.renderer, "gfx/ExitButton.png");		//Assign exit button to the image of exit button texture	
	SDL_Texture* forest_map_button;		//Initializing an SDL_Texture and naming it forest_map_button
	forest_map_button = IMG_LoadTexture(app.renderer, "gfx/MapForest.png");		//Assign forest_map_button to the image of MapForest texture
	SDL_Texture* volcano_map_button;		//Initializing an SDL_Texture and naming it volcano_map_button
	volcano_map_button = IMG_LoadTexture(app.renderer, "gfx/MapVolcano.png");	//Assign volcano_map_button to the image of MapVolcano texture

	SDL_Rect play_button_tickbox;		//Setting SDL_Rect for play button as tickboxes so that player can click on them
	play_button_tickbox.w = 250;		//Setting the width to be the same as image size which is 250
	play_button_tickbox.h = 72;			//Setting the height to be the same as image size which is 72
	play_button_tickbox.x = (SCREEN_WIDTH) / 2 - 94;		//Setting the x value to the correct place which corresponds to the image
	play_button_tickbox.y = (SCREEN_HEIGHT) / 2 - 24;		//Setting the y value to the correct place which corresponds to the image
	SDL_Rect map_button_tickbox;		//Setting SDL_Rect for map button as tickboxes so that player can click on them
	map_button_tickbox.w = 250;		//Setting the width to be the same as image size which is 250
	map_button_tickbox.h = 72;		//Setting the height to be the same as image size which is 72
	map_button_tickbox.x = (SCREEN_WIDTH) / 2 - 94;		//Setting the x value to the correct place which corresponds to the image
	map_button_tickbox.y = (SCREEN_HEIGHT) / 2 + 54;		//Setting the y value to the correct place which corresponds to the image
	SDL_Rect exit_button_tickbox;		//Setting SDL_Rect for exit button as tickboxes so that player can click on them
	exit_button_tickbox.w = 250;		//Setting the width to be the same as image size which is 250
	exit_button_tickbox.h = 72;		//Setting the height to be the same as image size which is 72
	exit_button_tickbox.x = (SCREEN_WIDTH) / 2 - 94;	//Setting the x value to the correct place which corresponds to the image
	exit_button_tickbox.y = (SCREEN_HEIGHT) / 2 + 132;		//Setting the y value to the correct place which corresponds to the image
	SDL_Rect map1_button_tickbox;		//Setting SDL_Rect for map1 button as tickboxes so that player can click on them
	map1_button_tickbox.w = 250;		//Setting the width to be the same as image size which is 250
	map1_button_tickbox.h = 72;		//Setting the height to be the same as image size which is 72
	map1_button_tickbox.x = (SCREEN_WIDTH) / 2 - 94;	//Setting the x value to the correct place which corresponds to the image
	map1_button_tickbox.y = (SCREEN_HEIGHT) / 2 - 24;		//Setting the y value to the correct place which corresponds to the image
	SDL_Rect map2_button_tickbox;		//Setting SDL_Rect for map2 button as tickboxes so that player can click on them
	map2_button_tickbox.w = 250;		//Setting the width to be the same as image size which is 250
	map2_button_tickbox.h = 72;		//Setting the height to be the same as image size which is 72
	map2_button_tickbox.x = (SCREEN_WIDTH) / 2 - 94;	//Setting the x value to the correct place which corresponds to the image
	map2_button_tickbox.y = (SCREEN_HEIGHT) / 2 + 54;		//Setting the y value to the correct place which corresponds to the image

	while (1)		//Setting up a while loop to run for the main menu
	{
		app.ms = 0;
		app.s = 0;
		app.m = 0;
		prepareScene();		//using the prepare scene function to clear the screen

		SDL_SetRenderDrawColor(app.renderer, 34, 139, 34, 0);		//Setting the background colour for the main menu
		SDL_RenderClear(app.renderer);		//Clearing the app.renderer target 
		SDL_ShowCursor(1);		//Allowing the cursor to be shown on the screen during the main menu
		drawText(575, 200, 255, 255, 255, TEXT_LEFT, "BURGER GAME");		//Writing the text on the Screen
		SDL_Event e;		//initializing a SDL_event named mouse
		SDL_Point mouse;	//initializing a SDL_Point

		while (SDL_PollEvent(&e) && app.start != 1)		//Setting a while loop to run till player selects an option
		{
			if (e.type == SDL_MOUSEMOTION)		//If statement to check for any mouse motion
			{
				mouse.x = e.motion.x;		//Overwrite mouse.x to e.mouse.x
				mouse.y = e.motion.y;		//Overwrite mouse.y to e.mouse.y
				break;		//break the while loop 
			}
			if (e.type == SDL_MOUSEBUTTONDOWN)		//If statement to check if any button is clicked
			{
				if (SDL_PointInRect(&mouse, &play_button_tickbox)) {	//If statement to check if the play button is clicked
					app.start = 1;		//overwriting app.start to 1, to start the game
					stage.pizzaFound = 0;		//setting the pizza to 0
					initStage();		//Calling the initStage function to start the game. 
					SDL_ShowCursor(0);	//Hide the cursor which the game is on 
				}
				if (SDL_PointInRect(&mouse, &map_button_tickbox)) {		//If statement to check if the map button is clicked
					app.start = 2;		//overwriting the app.start to 2, to open map selection menu
				}
				if (SDL_PointInRect(&mouse, &exit_button_tickbox)) {	//If statement to check if the exit button is clicked
					SDL_Quit();		//Quit the game
					return 0;	//return 0
				}
				break;		//break the while loop 
			}
			if (e.type == SDL_QUIT)		//this if statement is to allow the player to quit the game by closing the game window
			{
				SDL_Quit();		//Quit the game
				return 0;	//return 0
				break;		//break the while loop 
			}
		}
		if (SDL_PointInRect(&mouse, &play_button_tickbox)) {		//Setting if statement so that the play button darkens when the mouse is hover over it
			SDL_SetTextureColorMod(playButton, 150, 150, 150);		//Setting the colour of the playbutton
		}
		else if (SDL_PointInRect(&mouse, &map_button_tickbox)) {		//Setting if statement so that the map button darkens when the mouse is hover over it
			SDL_SetTextureColorMod(mapButton, 150, 150, 150);		//Setting the colour of the mapButton
		}
		else if (SDL_PointInRect(&mouse, &exit_button_tickbox)) {		//Setting if statement so that the exit button darkens when the mouse is hover over it
			SDL_SetTextureColorMod(exitButton, 150, 150, 150);		//Setting the colour of the exitButton
		}
		else {
			SDL_SetTextureColorMod(playButton, 255, 255, 255);		//Setting a colour of play button when mouse is not hover over it
			SDL_SetTextureColorMod(mapButton, 255, 255, 255);		//Setting a colour of map button when mouse is not hover over it
			SDL_SetTextureColorMod(exitButton, 255, 255, 255);		//Setting a colour of exit button when mouse is not hover over it

		}
		blit(playButton, (SCREEN_WIDTH / 2) - 94, (SCREEN_HEIGHT / 2) - 24, 0);		//Output the play button on screen
		blit(pizza, 650, 100, 0);		//Output the pizza texture
		blit(mapButton, (SCREEN_WIDTH / 2) - 94, (SCREEN_HEIGHT / 2) + 54, 0);		//Output the map button on screen
		blit(exitButton, (SCREEN_WIDTH / 2) - 94, (SCREEN_HEIGHT / 2) + 132, 0);	//Output the exit button on screen

		while (app.start == 1)		//Instead of while(1), making it while(app.start == 1), because the game should only run when play button is pressed
		{
			prepareScene();		
			doInput();

			app.delegate.logic();

			app.delegate.draw();

			presentScene();

			capFrameRate(&then, &remainder);

			if (app.timer == 1)		//when the game is running start the timer
			{
				app.ms++;		//increasing the milliseconds. 
				if (app.ms >= 60)		//if milliseconds reach 60, execute this statement
				{
					app.s++;		//increasing the seconds.
					app.ms = 0;		//start the milliseconds timer again
				}
				if (app.s >= 60)	//if seconds reach 60, execute this statement
				{
					app.m++;		//increasing the minutes.
					app.s = 0;		//start the seconds timer again
				}
			}
			app.timer = 1;
			
		}

		while (app.start == 2)		//Setting up a while loop to run for the map selection menu
		{
			prepareScene();		//using the prepare scene function to clear the screen

			

			SDL_SetRenderDrawColor(app.renderer, 255, 128, 0, 0);		//Setting the background colour for the map selection menu
			SDL_RenderClear(app.renderer);		//Clearing the app.renderer target 

			drawText(555, 200, 255, 255, 255, TEXT_LEFT, "MAP SELECTION");		//Writing the text on the Screen

			while (SDL_PollEvent(&e) && app.start != 1)		//Setting a while loop to run till player selects an option
			{
				if (e.type == SDL_MOUSEMOTION)		//If statement to check for any mouse motion
				{
					mouse.x = e.motion.x;		//Overwrite mouse.x to e.mouse.x
					mouse.y = e.motion.y;		//Overwrite mouse.y to e.mouse.y
					break;		//break the while loop
				}

				if (e.type == SDL_MOUSEBUTTONDOWN)		//If statement to check if any button is clicked
				{
					if (SDL_PointInRect(&mouse, &map1_button_tickbox)) {	//If statement to check if the map button is clicked
						app.level = 1;		//assigning app.level to 1, which will load the first map
						app.start = 0;		//assigning app.start to 0, which will show the previous menu 
					}
					if (SDL_PointInRect(&mouse, &map2_button_tickbox)) {
						app.level = 2;		//assigning app.level to 1, which will load the first map
						app.start = 0;		//assigning app.start to 0, which will show the previous menu 
					}
					if (SDL_PointInRect(&mouse, &exit_button_tickbox)) {
						app.start = 0;		//assigning app.start to 0, which will show the previous menu 
					}
					break;		//break the while loop
				}
				if (e.type == SDL_QUIT)		//this if statement is to allow the player to quit the game by closing the game window
				{
					SDL_Quit();		//Quit the game
					return 0;	//return 0
					break;		//break the while loop 
				}
			}
			if (SDL_PointInRect(&mouse, &map1_button_tickbox)) {		//Setting if statement so that the map1 button darkens when the mouse is hover over it
				SDL_SetTextureColorMod(forest_map_button, 150, 150, 150);		//Setting the colour of the map1 button
			}
			else if (SDL_PointInRect(&mouse, &map2_button_tickbox)) {		//Setting if statement so that the map2 button darkens when the mouse is hover over it
				SDL_SetTextureColorMod(volcano_map_button, 150, 150, 150);		//Setting the colour of the map2 button
			}
			else if (SDL_PointInRect(&mouse, &exit_button_tickbox)) {		//Setting if statement so that the exit button darkens when the mouse is hover over it
				SDL_SetTextureColorMod(exitButton, 150, 150, 150);		//Setting the colour of the exit button
			}
			else {
				SDL_SetTextureColorMod(forest_map_button, 255, 255, 255);		//Setting a colour of map1 button when mouse is not hover over it
				SDL_SetTextureColorMod(volcano_map_button, 255, 255, 255);		//Setting a colour of map2 button when mouse is not hover over it
				SDL_SetTextureColorMod(exitButton, 255, 255, 255);		//Setting a colour of exit button when mouse is not hover over it

			}
			blit(forest_map_button, (SCREEN_WIDTH / 2) - 94, (SCREEN_HEIGHT / 2) - 24, 0);		//Output the map1 button on screen
			blit(volcano_map_button, (SCREEN_WIDTH / 2) - 94, (SCREEN_HEIGHT / 2) + 54, 0);		//Output the map2 button on screen
			blit(exitButton, (SCREEN_WIDTH / 2) - 94, (SCREEN_HEIGHT / 2) + 132, 0);		//Output the exit button on screen
			presentScene();		//present the menu on the screen 

			capFrameRate(&then, &remainder);	//sets the frame rate of the main menu for refreshing it

		}

		presentScene();

		capFrameRate(&then, &remainder);
	}

	return 0;
}

static void capFrameRate(long* then, float* remainder)
{
	long wait, frameTime;

	wait = 16 + *remainder;

	*remainder -= (int)* remainder;

	frameTime = SDL_GetTicks() - *then;

	wait -= frameTime;

	if (wait < 1)
	{
		wait = 1;
	}

	SDL_Delay(wait);

	*remainder += 0.667;

	*then = SDL_GetTicks();
}
