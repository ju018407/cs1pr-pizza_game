/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static SDL_Texture* pete[4];	//increasing the buffer size to add more images
static SDL_Texture* peteL[3];	//Initializing SDL_Texture to add images for walking animations on left side
static SDL_Texture* peteR[3];	//Initializing SDL_Texture to add images for walking animations on right side
int left;		//This variable is to ensure that the animations run smooth by setting the correct FPS in the left direction
int right;		//This variable is to ensure that the animations run smooth by setting the correct FPS in the right direction
int direction = 2;		//This variable is to ensure that the character faces the correct direction when using 'A' or 'D'

void initPlayer(void)
{
	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));
	stage.entityTail->next = player;
	stage.entityTail = player;

	player->health = 1;

	pete[0] = loadTexture("gfx/pete02.png");		//Assigning pete02 image to pete[0]
	pete[1] = loadTexture("gfx/pete01.png");		//Assigning pete01 image to pete[1]
	pete[2] = loadTexture("gfx/pete_left_jump.png");		//Assigning pete_left_jump image to pete[2]
	pete[3] = loadTexture("gfx/pete_right_jump.png");		//Assigning pete_right_jump image to pete[3]
	peteL[0] = loadTexture("gfx/pete_left1.png");		//Assigning pete_left1 image to peteL[0]
	peteL[1] = loadTexture("gfx/pete_left2.png");		//Assigning pete_left2 image to peteL[1]
	peteL[2] = loadTexture("gfx/pete_left3.png");		//Assigning pete_left3 image to peteL[2]
	peteR[0] = loadTexture("gfx/pete_right1.png");		//Assigning pete_right1 image to peteR[0]
	peteR[1] = loadTexture("gfx/pete_right2.png");		//Assigning pete_right2 image to peteR[1]
	peteR[2] = loadTexture("gfx/pete_right3.png");		//Assigning pete_right3 image to peteR[2]


	player->texture = pete[0];

	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
}

void doPlayer(void)
{
	player->dx = 0;


	if (player->isOnGround)		//An if statement to ensure the character faces the correct direction
	{
		player->texture = pete[direction - 1];		//This stops the animation when not doing any movement. 
	}

	if (app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -PLAYER_MOVE_SPEED;
		direction = 1;		//overwrite direction by the value 1

		if (player->isOnGround)		//If statement to ensure the player is on ground
		{
			left = (left + 1) % 16;		//Doing some caculations to set the speed of frames of walking animation in left. 
			player->texture = peteL[left / 6];		//load all the images in peteL, making it a walking animation
		}		
	}

	if (app.keyboard[SDL_SCANCODE_D])
	{
		player->dx = PLAYER_MOVE_SPEED;
		direction = 2;		//overwrite direction by the value 2

		if (player->isOnGround)		//If statement to ensure the player is on ground
		{
			right = (right + 1) % 16;		//Doing some caculations to set the speed of frames of walking animation in right. 
			player->texture = peteL[(right / 6) + 3];		//load all the images in peteR, making it a walking animation
		}
		
	}

	if (app.keyboard[SDL_SCANCODE_SPACE] && player->isOnGround && player-> isJumped == 0)
		//An if statement for the first jump and changed the I button to Space for jumping
	{
		player->riding = NULL;

		player->dy = -20;

		player->texture = pete[direction + 1];		//load all the images in pete, making it a jumping animation
		playSound(SND_JUMP, CH_PLAYER);
	}

	if (app.keyboard[SDL_SCANCODE_SPACE] && player->isJumped == 1 && player->isOnGround == 0)
		//An if statement for the second jump and changed the I button to Space for jumping
	{
		player->riding = NULL;		//stopping the player's motion

		player->dy = -15;		//Setting the double jump height

		player->isJumped = 2;	//Setting the isJumped to 2
		playSound(SND_JUMP, CH_PLAYER);

	}

	if (app.keyboard[SDL_SCANCODE_I])	//Changing the Space button to I to reset the player's position
	{
		player->x = player->y = 0;

		app.keyboard[SDL_SCANCODE_SPACE] = 0;
	}

	if (app.keyboard[SDL_SCANCODE_LSHIFT] && player->isOnGround)		//An if statement to allow the user to sprint
	{
		if (app.keyboard[SDL_SCANCODE_A])		//An if statement to check the user pressed 'A'
		{
			player->dx = -PLAYER_MOVE_SPEED * 1.5;		//Increasing the player's speed by 1.5 times

			if (player->isOnGround)		//An if statement to ensure the player is on ground when sprinting
			{
				left = (left + 1) % 16;		//Doing some caculations to set the speed of frames of walking animation in left.
				player->texture = peteL[left / 6];		//load all the images in peteL, making it a walking animation
			}
		}

		if (app.keyboard[SDL_SCANCODE_D])
		{
			player->dx = PLAYER_MOVE_SPEED * 1.5;		//Increasing the player's speed by 1.5 times

			if (player->isOnGround)		//An if statement to ensure the player is on ground when sprinting
			{
				right = (right + 1) % 16;		//Doing some caculations to set the speed of frames of walking animation in right.
				player->texture = peteR[right / 6];		//load all the images in peteR, making it a walking animation
			}
		}
	}
}
