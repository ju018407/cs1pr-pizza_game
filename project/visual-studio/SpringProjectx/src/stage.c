/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static void logic(void);
static void draw(void);
static void drawHud(void);

void initStage(void)
{
	app.delegate.logic = logic;
	app.delegate.draw = draw;

	memset(&stage, 0, sizeof(Stage));

	stage.entityTail = &stage.entityHead;

	initEntities();

	initPlayer();

	initMap();
}

static void logic(void)
{
	doPlayer();

	doEntities();

	doCamera();
}

static void draw(void)
{
	if (app.level == 1)				//if first map is selected then load the following colours in background
	{
		SDL_SetRenderDrawColor(app.renderer, 173, 216, 255, 0);		//Loading this background colour in map1
	}

	if (app.level == 2)				//if second map is selected then load the following colours in background
	{
		SDL_SetRenderDrawColor(app.renderer, 192, 192, 192, 0);		//Loading this background colour in map2
	}
	SDL_RenderFillRect(app.renderer, NULL);

	drawMap();

	drawEntities();

	drawHud();
}

static void drawHud(void)
{
	SDL_Rect r;

	r.x = 0;
	r.y = 0;
	r.w = SCREEN_WIDTH;
	r.h = 35;

	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_BLEND);
	SDL_SetRenderDrawColor(app.renderer, 0, 0, 0, 196);

	if (stage.pizzaFound == stage.pizzaTotal)		//An if statement to show the victory message, once the user collects all the pizza
	{
		app.timer = 2;		//stop the timer
		drawText(575, 200, 255, 69, 0, TEXT_LEFT, "VICTORY!!!");		//Show the victory message
		drawText(575, 250, 255, 69, 0, TEXT_LEFT, "IT TOOK YOU %d MINUTES %d SECONDS!!!", app.m, app.s);		//Show the message the time they took to complete the game
		drawText(575, 300, 255, 69, 0, TEXT_LEFT, "PRESS E TO CONTINUE!!!");		//Show the message to inform user to press button so that the main menu opens
	}
	SDL_RenderFillRect(app.renderer, &r);
	SDL_SetRenderDrawBlendMode(app.renderer, SDL_BLENDMODE_NONE);

	drawText(SCREEN_WIDTH - 5, 5, 255, 255, 255, TEXT_RIGHT, "BURGER %d/%d", stage.pizzaFound, stage.pizzaTotal);
	drawText(SCREEN_WIDTH - 1100, 5, 255, 255, 255, TEXT_RIGHT, "TIMER %d:%d", app.m, app.s);		//Show the timer on the screen
}
